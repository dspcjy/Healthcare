﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Healthcare
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        ShowData myShowData = new ShowData();
        DispatcherTimer timerUI;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            Process current = Process.GetCurrentProcess();
            Process[] processes = Process.GetProcessesByName(current.ProcessName);
            foreach (Process process in processes)
            {
                if (process.Id != current.Id)
                {
                    if (process.MainModule.FileName == current.MainModule.FileName)

                    {
                        MessageBox.Show("程序已经运行！");
                        this.Close();
                        return;
                    }
                }
            }


            this.WindowStyle = System.Windows.WindowStyle.None;
            this.WindowState = System.Windows.WindowState.Maximized;            
            this.Topmost = true;
            this.Hide();
            //先调用其隐藏方法 然后再显示出来,这样就会全屏,且任务栏不会出现.
            //如果不加这句 可能会出现假全屏即任务栏还在下面.
            this.Show();

            //加载显示数据
            myShowData.Sentence = "休息3分钟";
            sentenceLabel.DataContext = myShowData;
            relaxTimeLabel.DataContext = myShowData;

            //打开定时器
            //用定时器更新动画
            timerUI = new DispatcherTimer();
            timerUI.Interval = TimeSpan.FromSeconds(1);
            timerUI.Tick += timerUI_Tick; //注册函数
            timerUI.Start();
        }

        int timeCnt = 0;
        int timeTemp = 0;
        bool showFlag2 = true;
        private void timerUI_Tick(object sender, EventArgs e)
        {
            //定时执行的内容            
            if (timeCnt >= 48*60 - 1) //45min+3min  计数最小单位是1s
            {
                timeCnt = 0;
            }
            else
            {
                timeCnt++;
            }

            if (timeCnt < 45*60)
            {
                this.Hide();
                timeTemp = 48 * 60 - timeCnt;
                myShowData.RelaxTime = "剩余休息时间：" + (timeTemp / 60).ToString().PadLeft(2, '0') + ":" + (timeTemp % 60).ToString().PadLeft(2, '0');
            }
            else
            {
                timeTemp = 48 * 60 - timeCnt;
                myShowData.RelaxTime = "剩余休息时间：" + (timeTemp / 60).ToString().PadLeft(2, '0') + ":" + (timeTemp % 60).ToString().PadLeft(2, '0');
                if (fullScreenFlag == true)
                {
                    this.Show();
                }                
            }
        }

        #region 全屏显示切换
        bool fullScreenFlag = true;
        //单击进入全屏
        private void topView_MouseDoubleClick(object sender, RoutedEventArgs e)
        {
            if (topView.WindowStyle == WindowStyle.SingleBorderWindow)
            {
                this.WindowStyle = System.Windows.WindowStyle.None;
                this.WindowState = System.Windows.WindowState.Maximized;
                this.Topmost = true;
                this.Hide();
                //先调用其隐藏方法 然后再显示出来,这样就会全屏,且任务栏不会出现.
                //如果不加这句,可能会出现假全屏即任务栏还在下面.
                this.Show();                
                fullScreenFlag = true;
            }
        }
        //按Esc键退出全屏
        private void topView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Escape)//Esc键
            {
                this.WindowState = WindowState.Normal;
                this.WindowStyle = WindowStyle.SingleBorderWindow;
                fullScreenFlag = false;
            }
        }
        #endregion

        #region 自动隐藏鼠标，全屏切换 
        bool showFlag = true;
        private void Window_MouseMove(object sender, MouseEventArgs e)
        {
            Point p;
            p = e.GetPosition((IInputElement)sender);

#if false //隐藏图标和全屏幕切换功能 
            if (p.Y < 50)
            {
                this.WindowStyle = WindowStyle.SingleBorderWindow;
                this.Topmost = false;
                Mouse.OverrideCursor = null;
                showFlag = false;
            }
            else
            {
                Thread.Sleep(100);
                if (showFlag == false && fullScreenFlag == true)
                {
                    this.WindowStyle = System.Windows.WindowStyle.None;
                    this.WindowState = System.Windows.WindowState.Maximized;
                    this.Topmost = true;
                    this.Hide();
                    //先调用其隐藏方法 然后再显示出来,这样就会全屏,且任务栏不会出现.
                    //如果不加这句 可能会出现假全屏即任务栏还在下面.
                    this.Show();                    
                    Mouse.OverrideCursor = Cursors.None;
                    showFlag = true;
                }                
            }
#endif
        }
        #endregion

        private void closeButton_Click(object sender, RoutedEventArgs e)
        {
            System.Environment.Exit(0);
        }

        private void cancelButton_Click(object sender, RoutedEventArgs e)
        {
            this.Hide();
        }
    }
}
