﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Healthcare
{
    class ShowData : INotifyPropertyChanged //.net定义的接口
    {
        private string sentence;

        public string Sentence
        {
            get
            {
                return this.sentence;
            }
            set
            {
                this.sentence = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("Sentence")); //触发事件
                }
            }
        }

        private string relaxTime;
        public string RelaxTime
        {
            get
            {
                return this.relaxTime;
            }
            set
            {
                this.relaxTime = value;
                if (PropertyChanged != null)
                {
                    PropertyChanged(this, new PropertyChangedEventArgs("RelaxTime")); //触发事件
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged; //实现接口
    }
}
